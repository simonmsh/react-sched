import React from 'react';
import ReactDOM from 'react-dom';

class Listings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {channelFilter:"*",
                        textFilter:"",
                        listingsData:[]};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);                      
    }

    handleSubmit(event) {
        //alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({[name]:value});
    }

    componentDidMount() {
        let url = 'https://sgurr-tv-listings-4.azurewebsites.net/service';

        fetch(url, {
            crossDomain:true,
            method: 'GET',
            headers: {'Content-Type':'application/json'}
          })
            .then(response => response.json())
            .then(responseJson => {
                console.log(responseJson);
                //alert(responseJson);
                this.setState({listingsData : responseJson});
            });
    }

    render() {
        var filteredListingsData = this.state.listingsData;

        var searchString = this.state.textFilter.toLowerCase();
        if(searchString.length > 0){
            filteredListingsData = filteredListingsData.filter(l => {
                return l.title.toLowerCase().match(searchString) || l.desc.toLowerCase().match(searchString);
            });
        }

        if(this.state.channelFilter != "*"){
            filteredListingsData = filteredListingsData.filter(l => {
                return l.channel.startsWith(this.state.channelFilter);
            });
        }
        
        return(
            <div class="container-fluid">
                <h1>Schedules</h1>
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="form-group col-lg-2">
                            <label htmlFor="textFilter">Search</label>
                            <input name="textFilter" value={this.state.textFilter} type="text" className="form-control input-small" onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-lg-1">
                            <label htmlFor="channelFilter">Channel</label>
                            <select name="channelFilter" value={this.state.channelFilter} type="text" className="form-control input-small" onChange={this.handleChange} item-width="20">
                                <option value="*">*</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                                <option value="D">D</option>
                                <option value="E">E</option>
                                <option value="F">F</option>
                                <option value="G">G</option>
                                <option value="H">H</option>
                                <option value="I">I</option>
                                <option value="J">J</option>
                                <option value="K">K</option>
                                <option value="L">L</option>
                                <option value="M">M</option>
                                <option value="N">N</option>
                                <option value="O">O</option>
                                <option value="P">P</option>
                                <option value="Q">Q</option>
                                <option value="R">R</option>
                                <option value="S">S</option>
                                <option value="T">T</option>
                                <option value="U">U</option>
                                <option value="V">V</option>
                                <option value="W">W</option>
                                <option value="X">X</option>
                                <option value="Y">Y</option>
                                <option value="Z">Z</option>
                            </select>
                        </div>
                    </div>
                </form>            
                <table className="table table-striped table-bordered table-condensed">
                    <tbody>{filteredListingsData.map((item, key) => {
                        return (
                            <tr key = {key}>
                                <td>{item.start}<br/>{item.channel}</td>
                                <td>{item.title}<br/>{item.yr}</td>
                                <td>{item.desc}</td>
                            </tr>
                            )
                        })}
                    </tbody>
               </table>
               <a href="https://gitlab.com/simonmsh/react-sched">Source code</a>
            </div>
            );
    }
}

export default Listings;